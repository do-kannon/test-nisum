package com.nisum.main.exceptions;

public class DuplicateMailException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public DuplicateMailException() {
		super();
	}
}
