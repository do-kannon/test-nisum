package com.nisum.main.util;

import org.springframework.stereotype.Component;

@Component
public class Message {

	private String mensaje;
	
	public Message() {
		
	}
	
	public Message(String mensaje) {
		this.mensaje =mensaje;
	}
	
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
}
