package com.nisum.main.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

@Component
public class ValidationUtil {

	private static final String mailreg = "^[A-Za-z0-9+_.-]+@(.+)$";
	private static final String passreg = "^(?=\\w*\\d{2,})(?=\\w*[A-Z])(?=\\w*[a-z])\\S{3,}$";
	
	public Boolean isMailValid(String mail) {
		Pattern mailPatern = Pattern.compile(mailreg, Pattern.CASE_INSENSITIVE);
		Matcher mailmatcher = mailPatern.matcher(mail);

		if (!mailmatcher.matches()) {
			return false;
		}

		return true;
	}
	
	public Boolean isPasswordValid(String password) {
		 Pattern passPatern = Pattern.compile(passreg);
		 Matcher passmatcher = passPatern.matcher(password);
	
		 if(!passmatcher.matches()) {
		 return false;
		 }
		
		return true;
	}
}