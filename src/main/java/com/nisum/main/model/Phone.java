package com.nisum.main.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.SequenceGenerator;


@Entity
public class Phone implements Serializable {
	
		private static final long serialVersionUID = 3570365299102032601L;
		@Id
		@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "phone_sequence")
		@SequenceGenerator(name = "phone_sequence", sequenceName = "phone_sequence")
		private Integer id;

		private Integer number;
		
		private Integer citycode;
		
		private Integer contrycode;
		
		@JoinColumn(name = "user_id", referencedColumnName = "idUser")
		private User user;

		public Phone() {
			
		}

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}

		public Integer getNumber() {
			return number;
		}

		public void setNumber(Integer number) {
			this.number = number;
		}

		public Integer getCitycode() {
			return citycode;
		}

		public void setCitycode(Integer citycode) {
			this.citycode = citycode;
		}

		public Integer getCountrycode() {
			return contrycode;
		}

		public void setCountrycode(Integer countrycode) {
			this.contrycode = countrycode;
		}
}
