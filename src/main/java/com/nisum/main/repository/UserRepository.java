package com.nisum.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.nisum.main.model.User;
@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

	@Query(value = "SELECT * FROM USER WHERE EMAIL = :email",nativeQuery = true)
	public User getMailByString(@Param("email") String email);
	
	@Query(value = "SELECT * FROM USER WHERE NAME = :name and PASSWORD = :password",nativeQuery = true)
	public User getUserByNameAndPass(@Param("name") String name,@Param("password") String password);
}
