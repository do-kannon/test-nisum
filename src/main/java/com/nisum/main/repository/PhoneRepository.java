package com.nisum.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nisum.main.model.Phone;
@Repository
public interface PhoneRepository extends JpaRepository<Phone, Integer>{
	
}
