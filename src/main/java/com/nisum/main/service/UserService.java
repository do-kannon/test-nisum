package com.nisum.main.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nisum.main.dto.UserDTO;
import com.nisum.main.exceptions.DuplicateMailException;
import com.nisum.main.exceptions.InvalidLogin;
import com.nisum.main.exceptions.InvalidMailDomain;
import com.nisum.main.exceptions.InvalidPassword;
import com.nisum.main.model.User;
import com.nisum.main.repository.PhoneRepository;
import com.nisum.main.repository.UserRepository;
import com.nisum.main.util.ValidationUtil;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	PhoneRepository phoneRepository;
	
	@Autowired
	ValidationUtil validationUtil;

	public UserDTO user(User user) throws DuplicateMailException,InvalidPassword,InvalidMailDomain{
		UserDTO userDTO=null;
		
			System.out.println(user.getEmail());
			if(!validationUtil.isMailValid(user.getEmail())) {
				throw new InvalidMailDomain();
			}
			if(!validationUtil.isPasswordValid(user.getPassword())) {
				throw new InvalidPassword();
			}
			User u = userRepository.getMailByString(user.getEmail());
			
			if(null != u) {
				throw new DuplicateMailException();
			}else {
				user.setCreated(new Date());
				user.setModified(new Date());
				user.setIsactive(true);
				userRepository.save(user);
				userDTO = new UserDTO();
				userDTO.setId(user.getIdUser());
				userDTO.setCreated(new Date());
				userDTO.setIsactive(true);
				userDTO.setModified(new Date());
				userDTO.setToken(user.getToken());
			}
		

		return userDTO;
	}
	
	public User login(String name,String password) throws InvalidLogin {
		
		User user = userRepository.getUserByNameAndPass(name, password);
		if(null != user ){
			user.setLast_login(new Date());
			userRepository.save(user);
		}else {
			throw new InvalidLogin();
		}
		return user;
	}

}
