package com.nisum.main.controller;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nisum.main.dto.UserDTO;
import com.nisum.main.exceptions.DuplicateMailException;
import com.nisum.main.exceptions.InvalidLogin;
import com.nisum.main.exceptions.InvalidMailDomain;
import com.nisum.main.exceptions.InvalidPassword;
import com.nisum.main.model.User;
import com.nisum.main.service.UserService;
import com.nisum.main.util.Message;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
@RequestMapping("nisum")
public class UserController {

	@Autowired
	UserService service;

	@PostMapping("/user")
	public ResponseEntity<?> user(@RequestBody User user) {
		UserDTO userDTO = null;

		try {
			String token = user.getName();
			user.setToken(getJWTToken(token));
			userDTO = service.user(user);
		} catch (DuplicateMailException e) {
			return new ResponseEntity<Message>(new Message("El correo ya esta registrado"), HttpStatus.CONFLICT);
		} catch (InvalidPassword e) {
			return new ResponseEntity<Message>(new Message("Password invalido"), HttpStatus.CONFLICT);
		} catch (InvalidMailDomain e) {
			return new ResponseEntity<Message>(new Message("Email invalido"), HttpStatus.CONFLICT);
		} catch (Exception e) {
			return new ResponseEntity<Message>(new Message("Error en el servidor"), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<UserDTO>(userDTO, HttpStatus.CREATED);

	}
	
	@PostMapping("/login")
	public ResponseEntity<?> login (@RequestParam("name") String name,@RequestParam("password")String password) {
		User user = null;
		try {
			user = service.login(name, password);
		}catch(InvalidLogin e) {
			return new ResponseEntity<String>("Login incorrecto",HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<User>(user,HttpStatus.OK);
	}
	
	private String getJWTToken(String username) {
		String secretKey = "mySecretKey";
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");

		String token = Jwts.builder().setId("softtekJWT").setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000))
				.signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();

		return "Bearer " + token;
	}

}
